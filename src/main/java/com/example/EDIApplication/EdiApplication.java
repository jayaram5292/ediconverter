package com.example.EDIApplication;

import io.xlate.edi.stream.EDIInputFactory;
import io.xlate.edi.stream.EDIStreamEvent;
import io.xlate.edi.stream.EDIStreamException;
import io.xlate.edi.stream.EDIStreamReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;

@SpringBootApplication
public class EdiApplication {

	public static void main(String[] args) throws IOException, EDIStreamException {
		SpringApplication.run(EdiApplication.class, args);
		File file = new File(
				"uploaded_txt_files/20220805.19424199.txt");
		// Creating an object of BufferedReader class
		InputStream stream = new FileInputStream(file);
		EDIInputFactory factory = EDIInputFactory.newFactory();
		EDIStreamReader reader = factory.createEDIStreamReader(stream);
		while (reader.hasNext()) {
			switch (reader.next()) {
				case START_INTERCHANGE:
					String standard = reader.getStandard();
					String[] version = reader.getVersion();
					System.out.println("hasNext" + reader.hasNext());
					System.out.println("Start_Interchange_Next:"+reader.next());
					System.out.println("Start_Interchange_test::"+reader.getText());
					break;
				case START_SEGMENT:
					// Retrieve the segment name - e.g. "ISA" (X12), "UNB" (EDIFACT), or "STX" (TRADACOMS)
					String segmentName = reader.getText();
					System.out.println("hasNext" + reader.hasNext());
					System.out.println("Start_Segment_Next:"+reader.next());
					System.out.println("Start_Segment_test::"+reader.getText());
					break;

				case END_SEGMENT:
					System.out.println("End_Segment_Next:"+reader.next());

					System.out.println("End_Segment_test::"+reader.getText());
					break;

				case START_COMPOSITE:
					System.out.println("hasNext" + reader.hasNext());
					System.out.println("Start_Composite_Next:"+reader.next());
					System.out.println("Start_Composite_test::"+reader.getText());
					break;

				case END_COMPOSITE:
					System.out.println("End_Composite_Next:"+reader.next());
					System.out.println("End_Composite_test::"+reader.getText());
					break;

				case ELEMENT_DATA:
					// Retrieve the value of the current element
					String data = reader.getText();
					//System.out.println("Next:"+reader.next());
					System.out.println("Element_Data_test::"+reader.getText());
					break;

			}
		}
		reader.close();
		stream.close();
	}

}

package com.example.EDIApplication.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value = "")
public class Response {
    private String errorMessage;

    private Boolean success;

    public Response(String errorMessage){
        this.errorMessage = errorMessage;
    }
}

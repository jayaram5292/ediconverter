package com.example.EDIApplication.controller;

import com.example.EDIApplication.dto.Response;
import com.example.EDIApplication.service.EDIReaderService;
import io.xlate.edi.stream.EDIInputFactory;
import io.xlate.edi.stream.EDIStreamException;
import io.xlate.edi.stream.EDIStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.*;

@RestController
public class EDIReaderController {

    private static final Logger logger = LoggerFactory.getLogger(EDIReaderController.class);

    @Autowired
    EDIReaderService ediReaderService;

    @CrossOrigin("*")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<?> uploadEDIFiles(@RequestBody MultipartFile ediFile) throws IOException, EDIStreamException, XMLStreamException, TransformerException, ParserConfigurationException {

        if(!ediFile.isEmpty()){
            return ediReaderService.processEdiFile(ediFile);
        }
        else{
            String errorMessage = "File must be not null or empty";
            logger.info(errorMessage);
            return new ResponseEntity<>(new Response(errorMessage), HttpStatus.BAD_REQUEST);
        }
    }
}

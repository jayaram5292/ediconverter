package com.example.EDIApplication.service;
import io.xlate.edi.stream.EDIInputFactory;
import io.xlate.edi.stream.EDIStreamException;
import io.xlate.edi.stream.EDIStreamReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

@Service
public class EDIReaderService {

    @Value("${ediPath}")
    private String localFolder;

    public ResponseEntity<?> processEDIFiles(MultipartFile ediFile) throws EDIStreamException, IOException, XMLStreamException, TransformerException {


        // Note:  Double backquote is to avoid compiler
        // interpret words
        // like \test as \t (ie. as a escape sequence)

        // Creating an object of BufferedReader class
        File file = saveFileIntoLocal(ediFile, localFolder);
        InputStream stream = new FileInputStream(file);
        EDIInputFactory ediFactory = EDIInputFactory.newFactory();
        XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
        //InputStream stream = getClass().getResourceAsStream("/x12/optionalInterchangeServices.edi");
        ediFactory.setProperty(EDIInputFactory.XML_DECLARE_TRANSACTION_XMLNS, Boolean.TRUE);
        EDIStreamReader reader = ediFactory.createEDIStreamReader(stream);
        EDIStreamReader filtered = ediFactory.createFilteredReader(reader, r -> true);
        XMLStreamReader xmlReader = ediFactory.createXMLStreamReader(filtered);
        xmlReader.next(); // Per StAXSource JavaDoc, put in START_DOCUMENT state
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter result = new StringWriter();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        transformer.transform(new StAXSource(xmlReader), new StreamResult(outputStream));
        String resultString = outputStream.toString();
        System.out.println(resultString);
        // deleteFileFromLocal(file);
        return new ResponseEntity<>(new ByteArrayResource(outputStream.toByteArray()), HttpStatus.OK);
    }


    public File saveFileIntoLocal(MultipartFile file, String path) {
        try {
            makeDirectory(path);
            InputStream is = file.getInputStream();
            Files.copy(is, Paths.get(String.format("%s%s", path, file.getOriginalFilename())), StandardCopyOption.REPLACE_EXISTING);
            return new File(String.format("%s%s", path, file.getOriginalFilename()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private File makeDirectory(String path) {
        File localFile = new File(path);
        boolean isCreated = localFile.exists();
        if (!isCreated) isCreated = localFile.mkdir();
        //logger.info(String.format("Local directory with this path %s is created.%s", path, isCreated));
        return localFile;
    }

    private EDIStreamReader ediReader;

    public boolean deleteFileFromLocal(File file) {
        return file != null && file.exists() && file.delete();
    }

    public ResponseEntity<?> processEdiFile(MultipartFile ediFile) throws IOException, EDIStreamException, ParserConfigurationException {
        File file = saveFileIntoLocal(ediFile, localFolder);
        BufferedReader br = new BufferedReader(new FileReader(file));
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Boolean s5Loop = false;
        ArrayList<String>normalString = new ArrayList<>();
        ArrayList<String>loopString1 = new ArrayList<>();
        ArrayList<String>loopString2 = new ArrayList<>();
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // add elements to Document
            Element mainElement = doc.createElement("Interchange");
            doc.appendChild(mainElement);
            String st;
            int loop = 0;
            while ((st = br.readLine()) != null) {
                if ((st.startsWith("SE")) || (st.startsWith("GE")) || (st.startsWith("IEA"))) {
                    break;
                }
                System.out.println(st);
                s5Loop= getS5Instance(st,s5Loop);
                if (s5Loop) {
                    if (st.startsWith("S5")) {
                        loop++;
                    }
                    if (loop == 1) {
                        loopString1.add(st.replace("~",""));
                    } else {
                        loopString2.add(st.replace("~",""));
                    }
                } else {
                    normalString.add(st.replace("~",""));
                }
            }
            for(String element : normalString){
                mainElement.appendChild(createNormalElement(doc,element));
            }
            mainElement.appendChild(createFirstLoop(doc,loopString1));
            mainElement.appendChild(createFirstLoop(doc,loopString1));
            transformXmlFile(doc,file.getName());
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private Node createFirstLoop(Document doc,ArrayList<String> loopString1) {
        Element loopElement = doc.createElement("S5Loop");
        for(String element : loopString1){
            loopElement.appendChild(createNormalElement(doc,element));
        }
        return loopElement;
    }

    private Boolean getS5Instance(String st, Boolean s5Loop) {
        String[] elements = st.split("\\*");
        switch (elements[0]){
            case "S5":
                s5Loop = true;
                break;
            case "L3":
                s5Loop = false;
                break;
            default:
                break;
        }
        return s5Loop;
    }

    private void transformXmlFile(Document doc, String name) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // for pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(localFolder + name));

            transformer.transform(source, console);
            transformer.transform(source, file);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Node createNormalElement(Document doc, String element) {
        int i = 0;
        Element rootElement = null;
        rootElement= createEDINormalTags(doc,rootElement,element,i);
        return rootElement;
    }

    private Element createEDINormalTags(Document doc, Element rootElement, String element, int i) {
        String[] elements = element.split("\\*");
        rootElement = doc.createElement(elements[0]);
        for (String seperateElement : elements) {
            if (i != 0) {
                rootElement.appendChild(createEDIElement(doc, elements[0] + i, elements[i]));
            }
            i++;
        }
        return rootElement;
    }

    private Node createEDIElement(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
}
